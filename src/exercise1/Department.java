/**
 */
package exercise1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link exercise1.Department#getName <em>Name</em>}</li>
 *   <li>{@link exercise1.Department#getCourses <em>Courses</em>}</li>
 *   <li>{@link exercise1.Department#getEmploys <em>Employs</em>}</li>
 *   <li>{@link exercise1.Department#getStudyprograms <em>Studyprograms</em>}</li>
 * </ul>
 *
 * @see exercise1.Exercise1Package#getDepartment()
 * @model
 * @generated
 */
public interface Department extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see exercise1.Exercise1Package#getDepartment_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link exercise1.Department#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' reference list.
	 * The list contents are of type {@link exercise1.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' reference list.
	 * @see exercise1.Exercise1Package#getDepartment_Courses()
	 * @model
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Employs</b></em>' reference list.
	 * The list contents are of type {@link exercise1.Employee}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Employs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Employs</em>' reference list.
	 * @see exercise1.Exercise1Package#getDepartment_Employs()
	 * @model
	 * @generated
	 */
	EList<Employee> getEmploys();

	/**
	 * Returns the value of the '<em><b>Studyprograms</b></em>' reference list.
	 * The list contents are of type {@link exercise1.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Studyprograms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Studyprograms</em>' reference list.
	 * @see exercise1.Exercise1Package#getDepartment_Studyprograms()
	 * @model
	 * @generated
	 */
	EList<StudyProgram> getStudyprograms();

} // Department
