/**
 */
package exercise1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link exercise1.Student#getStudyprograms <em>Studyprograms</em>}</li>
 *   <li>{@link exercise1.Student#getName <em>Name</em>}</li>
 *   <li>{@link exercise1.Student#getId <em>Id</em>}</li>
 *   <li>{@link exercise1.Student#getRegisteredcourseinstances <em>Registeredcourseinstances</em>}</li>
 *   <li>{@link exercise1.Student#getRegisteredevaluationform <em>Registeredevaluationform</em>}</li>
 * </ul>
 *
 * @see exercise1.Exercise1Package#getStudent()
 * @model
 * @generated
 */
public interface Student extends EObject {
	/**
	 * Returns the value of the '<em><b>Studyprograms</b></em>' reference list.
	 * The list contents are of type {@link exercise1.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Studyprograms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Studyprograms</em>' reference list.
	 * @see exercise1.Exercise1Package#getStudent_Studyprograms()
	 * @model
	 * @generated
	 */
	EList<StudyProgram> getStudyprograms();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see exercise1.Exercise1Package#getStudent_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link exercise1.Student#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see exercise1.Exercise1Package#getStudent_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link exercise1.Student#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Registeredcourseinstances</b></em>' reference list.
	 * The list contents are of type {@link exercise1.CourseInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registeredcourseinstances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registeredcourseinstances</em>' reference list.
	 * @see exercise1.Exercise1Package#getStudent_Registeredcourseinstances()
	 * @model
	 * @generated
	 */
	EList<CourseInstance> getRegisteredcourseinstances();

	/**
	 * Returns the value of the '<em><b>Registeredevaluationform</b></em>' reference list.
	 * The list contents are of type {@link exercise1.EvaluationForm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registeredevaluationform</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registeredevaluationform</em>' reference list.
	 * @see exercise1.Exercise1Package#getStudent_Registeredevaluationform()
	 * @model
	 * @generated
	 */
	EList<EvaluationForm> getRegisteredevaluationform();

} // Student
