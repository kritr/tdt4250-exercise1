/**
 */
package exercise1.impl;

import exercise1.Course;
import exercise1.CourseInstance;
import exercise1.CourseWork;
import exercise1.EvaluationForm;
import exercise1.Exercise1Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link exercise1.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getCourseinstances <em>Courseinstances</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getRequiredFor <em>Required For</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getRecommendedFor <em>Recommended For</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getEvaluations <em>Evaluations</em>}</li>
 *   <li>{@link exercise1.impl.CourseImpl#getCoursework <em>Coursework</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final int CREDITS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected int credits = CREDITS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourseinstances() <em>Courseinstances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseinstances()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> courseinstances;

	/**
	 * The cached value of the '{@link #getRequiredFor() <em>Required For</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredFor()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> requiredFor;

	/**
	 * The cached value of the '{@link #getRecommendedFor() <em>Recommended For</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecommendedFor()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> recommendedFor;

	/**
	 * The cached value of the '{@link #getEvaluations() <em>Evaluations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluations()
	 * @generated
	 * @ordered
	 */
	protected EList<EvaluationForm> evaluations;

	/**
	 * The cached value of the '{@link #getCoursework() <em>Coursework</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoursework()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseWork> coursework;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Exercise1Package.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(int newCredits) {
		int oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.COURSE__CREDITS, oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCourseinstances() {
		if (courseinstances == null) {
			courseinstances = new EObjectResolvingEList<CourseInstance>(CourseInstance.class, this, Exercise1Package.COURSE__COURSEINSTANCES);
		}
		return courseinstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRequiredFor() {
		if (requiredFor == null) {
			requiredFor = new EObjectResolvingEList<Course>(Course.class, this, Exercise1Package.COURSE__REQUIRED_FOR);
		}
		return requiredFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRecommendedFor() {
		if (recommendedFor == null) {
			recommendedFor = new EObjectResolvingEList<Course>(Course.class, this, Exercise1Package.COURSE__RECOMMENDED_FOR);
		}
		return recommendedFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EvaluationForm> getEvaluations() {
		if (evaluations == null) {
			evaluations = new EObjectResolvingEList<EvaluationForm>(EvaluationForm.class, this, Exercise1Package.COURSE__EVALUATIONS);
		}
		return evaluations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseWork> getCoursework() {
		if (coursework == null) {
			coursework = new EObjectResolvingEList<CourseWork>(CourseWork.class, this, Exercise1Package.COURSE__COURSEWORK);
		}
		return coursework;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Exercise1Package.COURSE__CODE:
				return getCode();
			case Exercise1Package.COURSE__NAME:
				return getName();
			case Exercise1Package.COURSE__CONTENT:
				return getContent();
			case Exercise1Package.COURSE__CREDITS:
				return getCredits();
			case Exercise1Package.COURSE__COURSEINSTANCES:
				return getCourseinstances();
			case Exercise1Package.COURSE__REQUIRED_FOR:
				return getRequiredFor();
			case Exercise1Package.COURSE__RECOMMENDED_FOR:
				return getRecommendedFor();
			case Exercise1Package.COURSE__EVALUATIONS:
				return getEvaluations();
			case Exercise1Package.COURSE__COURSEWORK:
				return getCoursework();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Exercise1Package.COURSE__CODE:
				setCode((String)newValue);
				return;
			case Exercise1Package.COURSE__NAME:
				setName((String)newValue);
				return;
			case Exercise1Package.COURSE__CONTENT:
				setContent((String)newValue);
				return;
			case Exercise1Package.COURSE__CREDITS:
				setCredits((Integer)newValue);
				return;
			case Exercise1Package.COURSE__COURSEINSTANCES:
				getCourseinstances().clear();
				getCourseinstances().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case Exercise1Package.COURSE__REQUIRED_FOR:
				getRequiredFor().clear();
				getRequiredFor().addAll((Collection<? extends Course>)newValue);
				return;
			case Exercise1Package.COURSE__RECOMMENDED_FOR:
				getRecommendedFor().clear();
				getRecommendedFor().addAll((Collection<? extends Course>)newValue);
				return;
			case Exercise1Package.COURSE__EVALUATIONS:
				getEvaluations().clear();
				getEvaluations().addAll((Collection<? extends EvaluationForm>)newValue);
				return;
			case Exercise1Package.COURSE__COURSEWORK:
				getCoursework().clear();
				getCoursework().addAll((Collection<? extends CourseWork>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Exercise1Package.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case Exercise1Package.COURSE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Exercise1Package.COURSE__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case Exercise1Package.COURSE__CREDITS:
				setCredits(CREDITS_EDEFAULT);
				return;
			case Exercise1Package.COURSE__COURSEINSTANCES:
				getCourseinstances().clear();
				return;
			case Exercise1Package.COURSE__REQUIRED_FOR:
				getRequiredFor().clear();
				return;
			case Exercise1Package.COURSE__RECOMMENDED_FOR:
				getRecommendedFor().clear();
				return;
			case Exercise1Package.COURSE__EVALUATIONS:
				getEvaluations().clear();
				return;
			case Exercise1Package.COURSE__COURSEWORK:
				getCoursework().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Exercise1Package.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case Exercise1Package.COURSE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Exercise1Package.COURSE__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case Exercise1Package.COURSE__CREDITS:
				return credits != CREDITS_EDEFAULT;
			case Exercise1Package.COURSE__COURSEINSTANCES:
				return courseinstances != null && !courseinstances.isEmpty();
			case Exercise1Package.COURSE__REQUIRED_FOR:
				return requiredFor != null && !requiredFor.isEmpty();
			case Exercise1Package.COURSE__RECOMMENDED_FOR:
				return recommendedFor != null && !recommendedFor.isEmpty();
			case Exercise1Package.COURSE__EVALUATIONS:
				return evaluations != null && !evaluations.isEmpty();
			case Exercise1Package.COURSE__COURSEWORK:
				return coursework != null && !coursework.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", name: ");
		result.append(name);
		result.append(", content: ");
		result.append(content);
		result.append(", credits: ");
		result.append(credits);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
