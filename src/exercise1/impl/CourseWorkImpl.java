/**
 */
package exercise1.impl;

import exercise1.CourseWork;
import exercise1.Exercise1Package;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Work</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link exercise1.impl.CourseWorkImpl#getTimeslot <em>Timeslot</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CourseWorkImpl extends MinimalEObjectImpl.Container implements CourseWork {
	/**
	 * The default value of the '{@link #getTimeslot() <em>Timeslot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeslot()
	 * @generated
	 * @ordered
	 */
	protected static final Date TIMESLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeslot() <em>Timeslot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeslot()
	 * @generated
	 * @ordered
	 */
	protected Date timeslot = TIMESLOT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseWorkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Exercise1Package.Literals.COURSE_WORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getTimeslot() {
		return timeslot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeslot(Date newTimeslot) {
		Date oldTimeslot = timeslot;
		timeslot = newTimeslot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.COURSE_WORK__TIMESLOT, oldTimeslot, timeslot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Exercise1Package.COURSE_WORK__TIMESLOT:
				return getTimeslot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Exercise1Package.COURSE_WORK__TIMESLOT:
				setTimeslot((Date)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Exercise1Package.COURSE_WORK__TIMESLOT:
				setTimeslot(TIMESLOT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Exercise1Package.COURSE_WORK__TIMESLOT:
				return TIMESLOT_EDEFAULT == null ? timeslot != null : !TIMESLOT_EDEFAULT.equals(timeslot);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timeslot: ");
		result.append(timeslot);
		result.append(')');
		return result.toString();
	}

} //CourseWorkImpl
