/**
 */
package exercise1.impl;

import exercise1.Course;
import exercise1.Department;
import exercise1.Employee;
import exercise1.Exercise1Package;
import exercise1.StudyProgram;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link exercise1.impl.DepartmentImpl#getName <em>Name</em>}</li>
 *   <li>{@link exercise1.impl.DepartmentImpl#getCourses <em>Courses</em>}</li>
 *   <li>{@link exercise1.impl.DepartmentImpl#getEmploys <em>Employs</em>}</li>
 *   <li>{@link exercise1.impl.DepartmentImpl#getStudyprograms <em>Studyprograms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DepartmentImpl extends MinimalEObjectImpl.Container implements Department {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourses() <em>Courses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> courses;

	/**
	 * The cached value of the '{@link #getEmploys() <em>Employs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmploys()
	 * @generated
	 * @ordered
	 */
	protected EList<Employee> employs;

	/**
	 * The cached value of the '{@link #getStudyprograms() <em>Studyprograms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyprograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyprograms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Exercise1Package.Literals.DEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.DEPARTMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourses() {
		if (courses == null) {
			courses = new EObjectResolvingEList<Course>(Course.class, this, Exercise1Package.DEPARTMENT__COURSES);
		}
		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Employee> getEmploys() {
		if (employs == null) {
			employs = new EObjectResolvingEList<Employee>(Employee.class, this, Exercise1Package.DEPARTMENT__EMPLOYS);
		}
		return employs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyprograms() {
		if (studyprograms == null) {
			studyprograms = new EObjectResolvingEList<StudyProgram>(StudyProgram.class, this, Exercise1Package.DEPARTMENT__STUDYPROGRAMS);
		}
		return studyprograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Exercise1Package.DEPARTMENT__NAME:
				return getName();
			case Exercise1Package.DEPARTMENT__COURSES:
				return getCourses();
			case Exercise1Package.DEPARTMENT__EMPLOYS:
				return getEmploys();
			case Exercise1Package.DEPARTMENT__STUDYPROGRAMS:
				return getStudyprograms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Exercise1Package.DEPARTMENT__NAME:
				setName((String)newValue);
				return;
			case Exercise1Package.DEPARTMENT__COURSES:
				getCourses().clear();
				getCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case Exercise1Package.DEPARTMENT__EMPLOYS:
				getEmploys().clear();
				getEmploys().addAll((Collection<? extends Employee>)newValue);
				return;
			case Exercise1Package.DEPARTMENT__STUDYPROGRAMS:
				getStudyprograms().clear();
				getStudyprograms().addAll((Collection<? extends StudyProgram>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Exercise1Package.DEPARTMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Exercise1Package.DEPARTMENT__COURSES:
				getCourses().clear();
				return;
			case Exercise1Package.DEPARTMENT__EMPLOYS:
				getEmploys().clear();
				return;
			case Exercise1Package.DEPARTMENT__STUDYPROGRAMS:
				getStudyprograms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Exercise1Package.DEPARTMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Exercise1Package.DEPARTMENT__COURSES:
				return courses != null && !courses.isEmpty();
			case Exercise1Package.DEPARTMENT__EMPLOYS:
				return employs != null && !employs.isEmpty();
			case Exercise1Package.DEPARTMENT__STUDYPROGRAMS:
				return studyprograms != null && !studyprograms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DepartmentImpl
