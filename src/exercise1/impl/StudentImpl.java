/**
 */
package exercise1.impl;

import exercise1.CourseInstance;
import exercise1.EvaluationForm;
import exercise1.Exercise1Package;
import exercise1.Student;
import exercise1.StudyProgram;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link exercise1.impl.StudentImpl#getStudyprograms <em>Studyprograms</em>}</li>
 *   <li>{@link exercise1.impl.StudentImpl#getName <em>Name</em>}</li>
 *   <li>{@link exercise1.impl.StudentImpl#getId <em>Id</em>}</li>
 *   <li>{@link exercise1.impl.StudentImpl#getRegisteredcourseinstances <em>Registeredcourseinstances</em>}</li>
 *   <li>{@link exercise1.impl.StudentImpl#getRegisteredevaluationform <em>Registeredevaluationform</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudentImpl extends MinimalEObjectImpl.Container implements Student {
	/**
	 * The cached value of the '{@link #getStudyprograms() <em>Studyprograms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyprograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyprograms;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRegisteredcourseinstances() <em>Registeredcourseinstances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredcourseinstances()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> registeredcourseinstances;

	/**
	 * The cached value of the '{@link #getRegisteredevaluationform() <em>Registeredevaluationform</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredevaluationform()
	 * @generated
	 * @ordered
	 */
	protected EList<EvaluationForm> registeredevaluationform;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Exercise1Package.Literals.STUDENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyprograms() {
		if (studyprograms == null) {
			studyprograms = new EObjectResolvingEList<StudyProgram>(StudyProgram.class, this, Exercise1Package.STUDENT__STUDYPROGRAMS);
		}
		return studyprograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.STUDENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Exercise1Package.STUDENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getRegisteredcourseinstances() {
		if (registeredcourseinstances == null) {
			registeredcourseinstances = new EObjectResolvingEList<CourseInstance>(CourseInstance.class, this, Exercise1Package.STUDENT__REGISTEREDCOURSEINSTANCES);
		}
		return registeredcourseinstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EvaluationForm> getRegisteredevaluationform() {
		if (registeredevaluationform == null) {
			registeredevaluationform = new EObjectResolvingEList<EvaluationForm>(EvaluationForm.class, this, Exercise1Package.STUDENT__REGISTEREDEVALUATIONFORM);
		}
		return registeredevaluationform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Exercise1Package.STUDENT__STUDYPROGRAMS:
				return getStudyprograms();
			case Exercise1Package.STUDENT__NAME:
				return getName();
			case Exercise1Package.STUDENT__ID:
				return getId();
			case Exercise1Package.STUDENT__REGISTEREDCOURSEINSTANCES:
				return getRegisteredcourseinstances();
			case Exercise1Package.STUDENT__REGISTEREDEVALUATIONFORM:
				return getRegisteredevaluationform();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Exercise1Package.STUDENT__STUDYPROGRAMS:
				getStudyprograms().clear();
				getStudyprograms().addAll((Collection<? extends StudyProgram>)newValue);
				return;
			case Exercise1Package.STUDENT__NAME:
				setName((String)newValue);
				return;
			case Exercise1Package.STUDENT__ID:
				setId((String)newValue);
				return;
			case Exercise1Package.STUDENT__REGISTEREDCOURSEINSTANCES:
				getRegisteredcourseinstances().clear();
				getRegisteredcourseinstances().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case Exercise1Package.STUDENT__REGISTEREDEVALUATIONFORM:
				getRegisteredevaluationform().clear();
				getRegisteredevaluationform().addAll((Collection<? extends EvaluationForm>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Exercise1Package.STUDENT__STUDYPROGRAMS:
				getStudyprograms().clear();
				return;
			case Exercise1Package.STUDENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Exercise1Package.STUDENT__ID:
				setId(ID_EDEFAULT);
				return;
			case Exercise1Package.STUDENT__REGISTEREDCOURSEINSTANCES:
				getRegisteredcourseinstances().clear();
				return;
			case Exercise1Package.STUDENT__REGISTEREDEVALUATIONFORM:
				getRegisteredevaluationform().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Exercise1Package.STUDENT__STUDYPROGRAMS:
				return studyprograms != null && !studyprograms.isEmpty();
			case Exercise1Package.STUDENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Exercise1Package.STUDENT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case Exercise1Package.STUDENT__REGISTEREDCOURSEINSTANCES:
				return registeredcourseinstances != null && !registeredcourseinstances.isEmpty();
			case Exercise1Package.STUDENT__REGISTEREDEVALUATIONFORM:
				return registeredevaluationform != null && !registeredevaluationform.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //StudentImpl
