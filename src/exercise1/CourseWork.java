/**
 */
package exercise1;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Work</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link exercise1.CourseWork#getTimeslot <em>Timeslot</em>}</li>
 * </ul>
 *
 * @see exercise1.Exercise1Package#getCourseWork()
 * @model abstract="true"
 * @generated
 */
public interface CourseWork extends EObject {
	/**
	 * Returns the value of the '<em><b>Timeslot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timeslot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timeslot</em>' attribute.
	 * @see #setTimeslot(Date)
	 * @see exercise1.Exercise1Package#getCourseWork_Timeslot()
	 * @model
	 * @generated
	 */
	Date getTimeslot();

	/**
	 * Sets the value of the '{@link exercise1.CourseWork#getTimeslot <em>Timeslot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timeslot</em>' attribute.
	 * @see #getTimeslot()
	 * @generated
	 */
	void setTimeslot(Date value);

} // CourseWork
