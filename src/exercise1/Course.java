/**
 */
package exercise1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link exercise1.Course#getCode <em>Code</em>}</li>
 *   <li>{@link exercise1.Course#getName <em>Name</em>}</li>
 *   <li>{@link exercise1.Course#getContent <em>Content</em>}</li>
 *   <li>{@link exercise1.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link exercise1.Course#getCourseinstances <em>Courseinstances</em>}</li>
 *   <li>{@link exercise1.Course#getRequiredFor <em>Required For</em>}</li>
 *   <li>{@link exercise1.Course#getRecommendedFor <em>Recommended For</em>}</li>
 *   <li>{@link exercise1.Course#getEvaluations <em>Evaluations</em>}</li>
 *   <li>{@link exercise1.Course#getCoursework <em>Coursework</em>}</li>
 * </ul>
 *
 * @see exercise1.Exercise1Package#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see exercise1.Exercise1Package#getCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link exercise1.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see exercise1.Exercise1Package#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link exercise1.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see exercise1.Exercise1Package#getCourse_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link exercise1.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(int)
	 * @see exercise1.Exercise1Package#getCourse_Credits()
	 * @model
	 * @generated
	 */
	int getCredits();

	/**
	 * Sets the value of the '{@link exercise1.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(int value);

	/**
	 * Returns the value of the '<em><b>Courseinstances</b></em>' reference list.
	 * The list contents are of type {@link exercise1.CourseInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstances</em>' reference list.
	 * @see exercise1.Exercise1Package#getCourse_Courseinstances()
	 * @model
	 * @generated
	 */
	EList<CourseInstance> getCourseinstances();

	/**
	 * Returns the value of the '<em><b>Required For</b></em>' reference list.
	 * The list contents are of type {@link exercise1.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required For</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required For</em>' reference list.
	 * @see exercise1.Exercise1Package#getCourse_RequiredFor()
	 * @model
	 * @generated
	 */
	EList<Course> getRequiredFor();

	/**
	 * Returns the value of the '<em><b>Recommended For</b></em>' reference list.
	 * The list contents are of type {@link exercise1.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recommended For</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recommended For</em>' reference list.
	 * @see exercise1.Exercise1Package#getCourse_RecommendedFor()
	 * @model
	 * @generated
	 */
	EList<Course> getRecommendedFor();

	/**
	 * Returns the value of the '<em><b>Evaluations</b></em>' reference list.
	 * The list contents are of type {@link exercise1.EvaluationForm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluations</em>' reference list.
	 * @see exercise1.Exercise1Package#getCourse_Evaluations()
	 * @model required="true"
	 * @generated
	 */
	EList<EvaluationForm> getEvaluations();

	/**
	 * Returns the value of the '<em><b>Coursework</b></em>' reference list.
	 * The list contents are of type {@link exercise1.CourseWork}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coursework</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coursework</em>' reference list.
	 * @see exercise1.Exercise1Package#getCourse_Coursework()
	 * @model
	 * @generated
	 */
	EList<CourseWork> getCoursework();

} // Course
